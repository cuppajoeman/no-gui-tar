import cairo
import math
from chord_generation import *

WIDTH = 1
HEIGHT = 4
PIXEL_SCALE = 200

TRUE_WIDTH = WIDTH*PIXEL_SCALE
TRUE_HEIGHT = HEIGHT*PIXEL_SCALE

FRETS = 24
STRINGS = 6

X_UNIT = WIDTH/(STRINGS-1)
Y_UNIT = HEIGHT/(FRETS-1)

#surface = cairo.ImageSurface(cairo.FORMAT_RGB24, TRUE_WIDTH , TRUE_HEIGHT)
surface = cairo.SVGSurface("scaled.svg", TRUE_WIDTH , TRUE_HEIGHT)

ctx = cairo.Context(surface)
ctx.scale(PIXEL_SCALE, PIXEL_SCALE)

ctx.select_font_face("Arial", cairo.FONT_SLANT_NORMAL, cairo.FONT_WEIGHT_NORMAL)

# BACKGROUND    
ctx.rectangle(0, 0, WIDTH, HEIGHT)
ctx.set_source_rgb(0, 0, 0)
ctx.fill()

# PREPARE BRUSH
ctx.set_source_rgb(1,1,1)
ctx.set_line_width(0.01)


# DRAWING

for i in range(FRETS):
    # TODO fix the scale
    scale = .5 *  (FRETS + 9)/(i+1)
    y_pos = HEIGHT/(FRETS - 1) * i
    scaled_y_pos = y_pos * scale
    ctx.move_to(0, y_pos)
    ctx.line_to(WIDTH, y_pos)
ctx.stroke()

for i in range(STRINGS):
    # We use -1, because we only want 5 gaps, which gives 6 strings
    x_pos = WIDTH/(STRINGS - 1) * i
    ctx.move_to(x_pos, 0)
    #ctx.arc(x_pos,0 ,15, 0, 2*math.pi)
    ctx.line_to(x_pos, HEIGHT)

ctx.stroke()


def show_pos_on_fretboard(x, y, interval, ctx):
    ctx.set_source_rgb(1, 1, 1)
    x_pos = x * X_UNIT 
    # We have to do this because the first fret isn't visible
    y_pos = (y+1) * Y_UNIT
    # DRAW CIRCLE
    rad = min(X_UNIT, Y_UNIT)/4
    shifted_y = y_pos - Y_UNIT/4
    ctx.arc(x_pos,shifted_y ,rad , 0, 2*math.pi)
    ctx.fill()


    ctx.set_font_size(rad)
    (tx, ty, width, height, dx, dy) = ctx.text_extents(interval)
    ctx.set_source_rgb(0, 0, 0)
    ctx.move_to(x_pos - width/2,shifted_y + height/2)
    ctx.show_text(interval)

def show_mult_pos_on_fb(list_of_positions, ctx):
    for pos in list_of_positions:
        # String representation is backwards
        print(pos)
        x = STRINGS - 1 - pos[0]
        y = pos[1]
        interval = pos[2]
        show_pos_on_fretboard(x, y,interval, ctx)

#show_pos_on_fretboard(1, 1, ctx)


# C7
positions = chord_constructor(9, [0, 4, 7, 10], False)
# E7
positions = chord_constructor(0, [0, 4, 7, 10], False)

show_mult_pos_on_fb(positions, ctx)


