import random

NUM_FRETS = 24
NUM_STRINGS = 6
BASE_EQUIVALENCE_PATTERN = [0, -5, 2, -3, 5, 0]
FRETBOARD = [["—"] * NUM_FRETS for _ in range(6)]

# CHORDS

MAJ_SEVENTH_CHORD_INTERVALS = [0, 4, 7, 11]
MIN_SEVENTH_CHORD_INTERVALS = [0, 3, 7, 10]
DOM_SEVENTH_CHORD_INTERVALS = [0, 4, 7, 10]
DIM_SEVENTH_CHORD_INTERVALS = [0, 3, 6, 9]

HALF_DIMINISHED_SEVENTH_CHORD_INTERVALS = [0, 3, 6, 10]
MINOR_MAJOR_SEVENTH_CHORD_INTERVALS = [0, 3, 7, 11]
AUGMENTED_MAJOR_SEVENTH_CHORD_INTERVALS = [0, 4, 8, 10]

MIN_SEVENTH_FLAT_FIVE = [0, 3, 6, 10]
# Because 9 = 14 semitones, flat 9, 13 and 13 % 12 = 1
DOM_SEVENTH_FLAT_NINE = [0, 4, 7, 10, 1]
DOM_SEVENTH_SHARP_NINE = [0, 4, 7, 10, 3]
SOMETHING_NEW = [0, 1, 7, 11, 3]

# Expand outward from the fingertips

EQUIVALENCE = [0]

NEARBY_INTERVALS = [
    4,5,6,
    11,0,1,
    6,7,8
]

MISSING_INTERVALS = [
    3,     
    10, 0  ,2,
            9
]

FURTHER_INTERVALS = [
    3,4,5,6,7,
    10,11,0,1,2,
    8,9
]

# LINES

LINE_Y_EQUALS_X = [0, 6]
LINE_Y_EQUALS_MINUS_X = [0, 4, 8]
# Because gcd(7, 12) = 1
LINE_Y_EQUALS_2X = [0, 7, 2, 9, 4, 11, 6, 1, 8, 3, 10, 5]
LINE_Y_EQUALS_MINUS_2X = [0,3,6,9]

LINE_Y_EQUALS_3X = [0, 8, 4]
LINE_Y_EQUALS_MINUS_3X = [0,2,4,6,8,10]



CHORDS = [
    EQUIVALENCE,
    NEARBY_INTERVALS,
    MISSING_INTERVALS,
    FURTHER_INTERVALS,
    LINE_Y_EQUALS_X,
    LINE_Y_EQUALS_MINUS_X,
    LINE_Y_EQUALS_2X,
    LINE_Y_EQUALS_MINUS_2X,
    LINE_Y_EQUALS_3X,
    LINE_Y_EQUALS_MINUS_3X,
    MAJ_SEVENTH_CHORD_INTERVALS,
    MIN_SEVENTH_CHORD_INTERVALS,
    DOM_SEVENTH_CHORD_INTERVALS,
    DIM_SEVENTH_CHORD_INTERVALS,
    HALF_DIMINISHED_SEVENTH_CHORD_INTERVALS,
    MINOR_MAJOR_SEVENTH_CHORD_INTERVALS,
    AUGMENTED_MAJOR_SEVENTH_CHORD_INTERVALS,
    MIN_SEVENTH_FLAT_FIVE,
    DOM_SEVENTH_FLAT_NINE,
    DOM_SEVENTH_SHARP_NINE,
    SOMETHING_NEW
    ]

def namestr(obj, namespace):
    return [name for name in namespace if namespace[name] is obj]

def print_mat(mat):
    print(' '.join())
    print('\n'.join([' '.join([str(cell) for cell in row]) for row in mat]))

    FRETBOARD = [["—"] * NUM_FRETS for _ in range(6)]

def print_mat_v2(matrix):
    #matrix = [[str(x) for x in range(NUM_FRETS)]] + matrix
    s = [[str(e) for e in row] for row in matrix]
    lens = [max(map(len, col)) for col in zip(*s)]
    fmt = ' '.join('{{:{}}}'.format(x) for x in lens)
    table = [fmt.format(*row) for row in s]
    print('\n'.join(table))

def equivalent_notes(n, symbol="X"):
    to_be_marked = []
    #Assuming s = 6
    s, f = n
    for i in range(NUM_STRINGS):
        shift = BASE_EQUIVALENCE_PATTERN[i]
        to_be_marked.extend(eqv_string(NUM_STRINGS-1-i, f + shift, symbol))
    return to_be_marked
    
def eqv_string(str_num, start_fret, symbol):
    to_be_marked = []
    while start_fret < 0:
        start_fret += 12
    rem = start_fret % 12
    fret_pos = rem
    while fret_pos <= NUM_FRETS-1:
      to_be_marked.append( (str_num, fret_pos, symbol))
      FRETBOARD[str_num][fret_pos] = symbol
      fret_pos += 12
    return to_be_marked

def chord_constructor(root, intervals, blank=False):
    to_be_marked = []
    for i in intervals:
        sym = 'X' if blank else str(i)
        to_be_marked.extend(equivalent_notes((5, root + i), sym))
    return to_be_marked

# MAIN

# Linear interval demonstration
from_six_pattern_across_strings = ''
mod_from_six_pattern_across_strings = ''
from_one_pattern_across_strings = ''
mod_from_one_pattern_across_strings = ''
string_intervals_six_to_one = [0,5, 5, 5, 4, 5]
string_intervals_one_to_six = [7, 7, 7, 8, 7, 0]

sum_down = 0
sum_up = 0
for i in range(6):
    sum_down += string_intervals_six_to_one[i]
    sum_up -= string_intervals_one_to_six[5-i]
    from_six_pattern_across_strings += (" " if i != 0 else '') + str(sum_down)
    from_one_pattern_across_strings =  str(sum_up) + " " + from_one_pattern_across_strings
    mod_from_six_pattern_across_strings += (" " if i != 0 else '') + str(sum_down % 12)
    mod_from_one_pattern_across_strings =  str(sum_up %12 - 12) + " " + mod_from_one_pattern_across_strings

#print("Patterns moving across strings, up and down in terms of semitones")
#print(from_six_pattern_across_strings,from_one_pattern_across_strings, sep='\n')
#print("Modulo 12")
#print(mod_from_six_pattern_across_strings,mod_from_one_pattern_across_strings, sep='\n')
#
##r_pos = random.randint(0, 12)
##equivalent_notes((5,r_pos), 'X')
##print_mat_v2(FRETBOARD)
##FRETBOARD = [["—"] * NUM_FRETS for _ in range(6)]
#
#
## settings for which ones will be blanks
#opts = [False for _ in CHORDS]
#opts[0] = True
##opts[1] = True
#
#count = 0
#for c in CHORDS:
#    # Randomize so player doesn't get unshifted pattern
#    title = namestr(c, globals())[0]
#    print(title)
#    if "INTERVAL" not in title:
#        r_pos = random.randint(0, 12)
#    chord_constructor(r_pos, c, opts[count])
#    print_mat_v2(FRETBOARD)
#    FRETBOARD = [["—"] * NUM_FRETS for _ in range(6)]
#    count += 1

